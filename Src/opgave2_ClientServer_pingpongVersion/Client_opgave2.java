package opgave2_ClientServer_pingpongVersion;


import java.io.*;
import java.net.*;
import java.util.Scanner;


public class Client_opgave2 {
	Socket requestSocket;
	ObjectOutputStream out;
	ObjectInputStream in;
	String message;
	BufferedReader input = new BufferedReader (new InputStreamReader (System.in));

	Client_opgave2(){

	}

	void run()
	{
		try{
			//1. Opret en socket

			// 	String serverIp = "10.10.140.61";
			//	String serverIp = "10.10.140.188";
			String serverIp = "localhost";

			int serverPort = 10000;

			System.out.println("Trying for " +serverIp + ":" + serverPort);
			requestSocket = new Socket(serverIp, serverPort);
			System.out.println("Forbundet til " +serverIp + ":" + serverPort);
			//2. get Input og Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			//3: Snak med serveren
			do{
				try{
					message = (String)in.readObject();
					System.out.println("server>" + message); 

					Scanner scanner = new Scanner (System.in);
					message = scanner.nextLine();
					sendMessage(message);

					sendMessage("Lars");

				}
				catch(ClassNotFoundException classNot){
					System.err.println("Data modtaget i ukendt format");
				}
			}while(!message.equals("Farvel"));
		}
		catch(UnknownHostException unknownHost){
			System.err.println("Forsøg på kobling til ukendt host!");
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}finally{
			//4: Luk forbindelsen
			try{
				in.close();
				out.close();
				requestSocket.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}
	void sendMessage(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
			System.out.println("Klient>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public static void main(String args[])
	{
		Client_opgave2 klient = new Client_opgave2();
		klient.run();
	}
}