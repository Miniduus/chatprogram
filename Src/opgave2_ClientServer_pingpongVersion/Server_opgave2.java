package opgave2_ClientServer_pingpongVersion;


import java.io.*;
import java.net.*;

public class Server_opgave2 {
	ServerSocket providerSocket;
	Socket connection = null;
	ObjectOutputStream out;
	ObjectInputStream in;
	String message;
	String tekst;

	Server_opgave2() {

	}

	void run() {
		try{
			//1. Opret en socket


			providerSocket = new ServerSocket(10000, 10);
			//2. Vent på forbindelse fra klient
			System.out.println("Venter på forbindelse");
			connection = providerSocket.accept();
			String clientIp = connection.getInetAddress().getHostName();
			System.out.println("Forbindelse fra " +  clientIp);
			//3. Input and Output streams
			out = new  	ObjectOutputStream(connection.getOutputStream());
			out.flush();
			in = new ObjectInputStream(connection.getInputStream());
			sendMessage("Forbindelse accepteret");

			do {
				try {
					message = (String)in.readObject();
					if (message.equals("Farvel")) {
						sendMessage("Farvel");
					}
					else {
						System.out.println("<" + clientIp + "> (client)> "  + message);
					}
				}
				catch(ClassNotFoundException classnot){
					System.err.println("Data modtaget i ukendt format");
				}
			}

			while(!message.equals("Farvel"));

		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}finally{
			try{
				//in.close();
				out.close();
				providerSocket.close();
			}
			catch(IOException ioException){
				ioException.printStackTrace();
			}
		}
	}
	void sendMessage(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	public static void main(String args[])
	{
		Server_opgave2 server = new Server_opgave2();
		while(true){
			server.run();
		}
	}
}