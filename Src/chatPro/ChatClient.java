package chatPro;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
 
public class ChatClient extends Thread{

	private String[]     serverIp   = {"Note:WillBeOverwritten" ,"localhost","c3.consult3.dk"}; 
	private final int    PORT       = 11000;
	private final String VERSION    = "Chat Pro Client v0.18c";
	private String userID;
	private static     Snippet sn    = Snippet.getInstance();
	private static String DELIMIT    = " ";

	private BufferedReader in;
	private PrintWriter out;
	private JFrame     frame           = new JFrame(VERSION);
	private JTextField tfClientMessage = new JTextField(40);
	private JTextArea  taMessages      = new JTextArea(8, 40);

	private JPanel     buttonPanel;
	private JButton[] btnClientsOnline = new JButton[4];
	private Socket     socket;

	public static void main(String[] args) throws Exception {
		ChatClient chatClient = new ChatClient("");
		chatClient.run();
	}

	public void msgToServer (String command, String text) {
		System.out.println(command + " " + text);
		out.println (command + " " + text);
	}

	private void setKeyListener (JButton key) { 
		key.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				out.println("REQFRIEND " + ((AbstractButton) e.getSource()).getText() );
			}
		});
	}

	public ChatClient(String userID) {
		this.userID = userID;
		tfClientMessage.setEditable(false);
		taMessages.setEditable(false);
		frame.getContentPane().add(tfClientMessage, "North");
		frame.getContentPane().add(new JScrollPane(taMessages), "Center");
		buttonPanel = new JPanel();
		for (int i=0; i<btnClientsOnline.length; i++) {
			btnClientsOnline[i] = new JButton("User"+i);
			btnClientsOnline[i].setVisible(false);

			buttonPanel.add(btnClientsOnline[i]);
			setKeyListener (btnClientsOnline[i]);
		}
		frame.getContentPane().add(buttonPanel , "South");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

		btnClientsOnline[2].setVisible(true);

		// new user message
		tfClientMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String message = getUserID() + " says "  +tfClientMessage.getText();
				out.println("MSG "+ getFriendsFromGreenButton() + "#" + message);
				clientLog( message) ;
				tfClientMessage.setText("");
			}
		});

		// handle shutdowns
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				out.println ("left");
			}
		}));
	}

	public String getUserID () {
		return this.userID;
	}

	public String getFriendsFromGreenButton() {
		String friends ="";
		for (int i = 0; i<btnClientsOnline.length;i++) {
			if (btnClientsOnline[i].getBackground().equals(Color.GREEN)) {
				friends+=btnClientsOnline[i].getText()+" ";
			}
		}
		return friends.trim();
	}

	public void startClient() throws IOException {
		run();
	}

	private String popUpWindow ( String message, String title) {
		return  JOptionPane.showInputDialog(frame, message, title,JOptionPane.PLAIN_MESSAGE);
	}

	public void connectToServer () throws Exception { 
		boolean isConnected = false;
		int i = 0;

		// connect to broadcastServer -> overwrite first serverIp[0]!!! 
		BroadCastClient client = new BroadCastClient();
		System.out.println("start fetching client.broadCastClient()");
		serverIp[0]  = client.broadCastClient();
		
		System.out.println("end fetching -> found : "+ serverIp[0]);
		
		while (i < serverIp.length && !isConnected) {
			clientLog("Connecting to " +serverIp[i] +":"+ PORT);
			socket = new Socket();

			try {
				socket.connect(new InetSocketAddress(serverIp[i], PORT), 5000);
				isConnected = true;
			} catch (IOException e) {
				clientLog("Failed connection to: " +serverIp[i] +":"+ PORT);
			}
			i++;
		}

		if (!isConnected) {
			clientLog("FATAL ERROR: Could not find servers. Now Shutting Down!");
			sn.shutDownSystem(3000);
		}

		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
	} 

	public void mainLoop() throws IOException {

		while (true) {
			String input = in.readLine();
			if (input == null) {
				return;
			}

			String command = sn.get1stHalf(input, DELIMIT) ;
			String value   = sn.get2ndHalf(input, DELIMIT) ;

			if (command.equals("LOGINPLEASE")) {
				if (userID.equals("")) {
					userID = popUpWindow ("Enter User Name",VERSION+ " Login");
				}
				out.println("LOGIN"+" " + userID + " " + "pwd" );
				frame.setTitle(userID + " - "+VERSION);
			}
			else if (command.equals("LOGINOK")) {
				//tfClientMessage.setEditable(true);
			} 
			else if (command.equals("MSG")) {
				clientLog(value) ;
			}
			else if (command.equals("REQFRIEND")) {
				reqFriend (value);
			}
			else if (command.equals("REQACCEPTED")) {
				changeButtonColor(value,Color.GREEN);
			}
			else if (command.equals("ONLINENOW")) {
				updateOnlineUsers(value);
			}
			else if (command.equals("KILL")) {
				clientLog("Server has shut down this session. Program will quit now");
				sn.shutDownSystem(3000);
			}
			else if (command.equals("LOG")) {
				clientLog(value);
			}
			else {
				clientLog( command +  " " + value );
			}
		}
	}

	public void reqFriend (String friendUserID) {
		int isReqAccepted = JOptionPane.showConfirmDialog(frame, "Do you accept a chat request from "+friendUserID, "chat Request",JOptionPane.YES_NO_OPTION);
		if (isReqAccepted == 0) {
			out.println("REQACCEPTED"+" "+ friendUserID);
		}
		else {
			out.println("REQDENIED" +" " + friendUserID);
		}
	}

	public void changeButtonColor(String friendUserID, Color color) {
		System.out.println("friendUserID:"+friendUserID);

		for (int i = 0; i<btnClientsOnline.length;i++) {
			if (friendUserID.equals(btnClientsOnline[i].getText(  ))) {
				btnClientsOnline[i].setBackground(color);
				btnClientsOnline[i].setOpaque(true);
				tfClientMessage.setEditable(true);

			}
		}
	}



	public void clientLog (String message) {
		taMessages.append(sn.getTimeStamp() + " "+ message + "\n");
	}

	public void updateOnlineUsers(String value) {
		String[] users =  value.split(" ");
		// hide button
		for (int i = 0; i<btnClientsOnline.length;i++) {
			btnClientsOnline[i].setVisible(false);
			tfClientMessage.setEditable(false);
		}

		//display all but self
		for (int i=0;  i<users.length; i++) {
			if (!users[i].equals(userID)) {
				btnClientsOnline[i].setText( users[i]);
				btnClientsOnline[i].setVisible(true);
				tfClientMessage.setEditable(false);
			}
		}
	}

	public void run() {
		try {
			connectToServer();
			mainLoop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}