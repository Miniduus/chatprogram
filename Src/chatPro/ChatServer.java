package chatPro;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class ChatServer {
	private static final String[] allowedUsersPasswords = {"Lars pwd","Steffen pwd", "Morten pwd", 
		"Stine pwd","Jens pwd","Tarek pwd"}; 

	private static final int       PORT = 11000;
	private static final String VERSION = "Chat Pro Server v0.18c"; 
	private static     Snippet sn       = Snippet.getInstance();

	private static Set<String> chatClientNames   = new HashSet<String>();
	private static Set<PrintWriter> chatClients  = new HashSet<PrintWriter>();

	private static Map <String, PrintWriter>  chatSessions = new TreeMap <String, PrintWriter>() ;
	private static Map <String, List<String>>  chatFriends = new TreeMap <String, List<String>>() ;

	private static JFrame frame ;
	private JTextField    command ; 
	private static JTextArea connectedClients ; 
	private JTextArea serverDebug ; 
	private static ChatServer server; 
	private static String DELIMITER = " ";

	public  ChatServer() {

		frame = new JFrame(VERSION);
		command = new JTextField(40);
		command.setEditable(true);

		connectedClients = new JTextArea(8, 40);
		connectedClients.setEditable(false);
		serverDebug = new JTextArea(8, 40);
		frame.getContentPane().add(command, "North");
		frame.getContentPane().add(new JScrollPane(connectedClients), "Center");
		frame.getContentPane().add(new JScrollPane(serverDebug), "South");
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		command.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				log(command.getText());
				ServerThread.writeAllClients(command.getText(),"");
				command.setText("");
			}
		});

		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			public void run() {
				ServerThread.writeAllClients("KILL","");
			}
		}));


	}

	public  void startServer () throws IOException {

		//start BroadCastServer
		Thread t1 = new Thread(new BroadCastServer());
		t1.start();
		
		//start Chat Server
		ServerSocket listener = null;
		try {
			System.out.println("Starting Server on port " + PORT);
			listener = new ServerSocket(PORT);
			log("Starting Server on port " + PORT);
		}
		catch (Exception e){
			log("Fatal Error:Could not get handle on port:" + PORT + " Now Shutting Down Server!");
			sn.shutDownSystem(5000);
		}
		log("Server Started on port " + PORT);

		try {
			while (true) {
				ServerThread st = new ServerThread(listener.accept());
				st.start();
				log("new Server Thread started id: " + st.getId());
			}
		} finally {
			listener.close();
		}
	}

	public static void main(String[] args) throws Exception {
		server = new ChatServer();
		server.startServer ();
	}

	public void log (String text) {
		serverDebug .append(sn.getTimeStamp() + " " + text + "\n");
	}
	public static void addClientTextArea(String text) {
		connectedClients.append(sn.getTimeStamp()  + " " + text + "\n");
	}

	private static class ServerThread extends Thread {
		private boolean        isUserLoggedin = false; 
		private String         userID;
		private Socket         socket;
		private BufferedReader in;
		private PrintWriter    out;
		//	private List <String>  friends; 

		public ServerThread(Socket socket) {
			this.socket = socket;

		}

		public boolean isUserIdAndPasswordOK (String userIdAndPassword) {
			for (int i=0; i < allowedUsersPasswords.length;i++ ) {
				if (userIdAndPassword.equals(allowedUsersPasswords[i])) {
					return true; 
				}
			}
			return false;  
		}

		public void run() {
			try {
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				EstablishCredentials();

				boolean isConnected = true;
				while (isConnected) {
					String input = in.readLine();
					if (input == null) {
						return;
					}

					System.out.println(userID + " says: "+input);
					String command = sn.get1stHalf(input, DELIMITER).toUpperCase();
					String value = sn.get2ndHalf(input, DELIMITER);


					if (command.equals( "MSG")) {
						writeToClients (sn.get1stHalf(value, "#"),  sn.get2ndHalf(value, "#"));
					}
					else if (command.equals( "WHOISONLINE")) {
						publishWhoIsOnline() ; 
					}
					else if (command.equals( "REQFRIEND")) {
						requestFriend (value );
					}
					else if (command.equals( "REQACCEPTED")) {
						addFriend (value );
					}
					else if (command.equals( "REQDENIED")) {
						writeToClient(value, "REQDENIED" + " " + userID);
					}
					else if (command.equals( "EXIT")) {
						isConnected = false;
						out.println("Goodbye "+userID);
					}
					else {
						out.println("ERROR. Command is unknown or badly formatted '"+command+"'");
					}
				}
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				disconnectClient();
			}	
		}

		public void disconnectClient(){
			if (userID != null) {
				chatClientNames.remove(userID);
				ChatServer.addClientTextArea(userID + " left"); 

			}
			if (out != null) {
				chatClients.remove(out);
			}
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("disconnectClient():" + e);
			}
			finally {
				publishWhoIsOnline();
			}
		}

		public void EstablishCredentials() throws IOException{
			int loginAttempts = 0;
			while (!isUserLoggedin) {

				loginAttempts++;
				out.println("LOGINPLEASE" + " "+VERSION + "") ;


				String input = in.readLine();
				if (input == null) {
					return;
				}

				String command = sn.get1stHalf (input, DELIMITER).toUpperCase();
				String value   = sn.get2ndHalf (input, DELIMITER);

				if (command.equals ("LOGIN")) {

					userID = sn.get1stHalf(value, DELIMITER);
					String userPassword = sn.get2ndHalf(value, DELIMITER); ;
					if (isUserIdAndPasswordOK(userID+DELIMITER+userPassword)) {
						out.println("LOGINOK");
						loginAttempts--;
						ChatServer.addClientTextArea(userID + " connected from " + socket.getInetAddress() + ":" + socket.getPort());

						synchronized (chatClientNames) {
							if (!chatClientNames.contains(userID)) {
								chatClientNames.add(userID);
								isUserLoggedin = true;
							}
							else {
								out.println("ERROR You are have already been logged in!");
							}
						}
					} 
					else {
						out.println("LOGINDENIED UserID/Password combination does not match or UserID unknown");
					}
				}
				else {
					out.println("ERROR COMMAND UNKNOWN: use LOGIN USERID PASSWORD or see TALK13T protocol");
				}
				if (loginAttempts==3) {
					out.println("LOG TOO MANY TRIES. YOU HAVE BEEN DISCONNECTED.");
					out.println("KILL");
					disconnectClient();
					return;
				}
			}
			chatClients.add(out);
			chatSessions.put(userID, out);
			chatFriends.put(userID, new ArrayList<String>());
			publishWhoIsOnline();
		}

		public static void writeAllClients(String command, String text) {
			for (PrintWriter writer : chatClients) {
				writer.println(command + " " + text);
			}
		}

		public void requestFriend (String friendsUserID) {
			if (!isFriend(friendsUserID)) {
				writeToClient(friendsUserID, "REQFRIEND " + userID);
			}
		}

		public void writeToClient (String friendsUserID, String message) {
			PrintWriter pw = chatSessions.get(friendsUserID);
			if (pw!=null) {
				pw.println(message);
			}
		}

		public void writeToClients (String friendsUserID, String message) {
			String[] users = friendsUserID.split(" ");

			for (int i=0; i<users.length; i++) {
				if (isFriend(users[i])) {
					writeToClient(users[i], "MSG " + message); // "REQFRIEND " + userID);
				}
				else {
					out.println("CHATDENIED Your Request to chat with '" + friendsUserID + "' is denied");
				}
			}
		} 

		public void  publishWhoIsOnline() {
			String onlineClients ="";
			for (String clients : chatClientNames) {
				onlineClients+=  clients + " " ;
			}
			writeAllClients("ONLINENOW", onlineClients.trim());
		}

		public void addFriend (String friendsUserID) {
			// confirm acceptance to all both
			PrintWriter pw = chatSessions.get(friendsUserID);
			pw.println("REQACCEPTED " + userID);
			pw = chatSessions.get(userID);
			pw.println("REQACCEPTED " + friendsUserID);

			// we are both friends
			if (!chatFriends.get(userID).contains(friendsUserID)) {
				chatFriends.get(userID).add(friendsUserID);
			}
			if (!chatFriends.get(friendsUserID).contains(userID)) {
				chatFriends.get(friendsUserID).add(userID);
			}
		}

		// code ready for version 0.2!
		@SuppressWarnings("unused")
		public void removeFriend (String friendsUserID) {
			//TODO
			if (chatFriends.get(userID).contains(friendsUserID)) {
				chatFriends.get(userID).remove(friendsUserID);
			}
			// REMEMBER TO DELETE BOTH FRIEND AND CURRENT USER

		}
		public boolean isFriend (String friendsUserID) {
			for (String f: chatFriends.get(userID)) {
				if (f.equals(friendsUserID)) {
					return true;
				}
			}
			return false;
		}
	}
}